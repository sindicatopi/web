import Head from 'next/head';

import 'src/styles.scss';

export default ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/icons/favicon_32x32.png" />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet" /> 
      </Head>
      <Component {...pageProps} />
    </>
  );
};
