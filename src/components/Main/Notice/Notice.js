import styles from './Notice.module.scss';

const Notice = () => (
  <section className={styles.notice}>
    <h3>Comunicado</h3>

    <h2>Mensaje de solidaridad por #BlackLivesMatter</h2>

    <p>
      Como sindicato queremos compartir algunos pensamientos e informaciones sobre lo que está ocurriendo actualmente en Estados Unidos, lo más probable es que ya estén al tanto del contexto de los hechos, sin embargo haremos una pequeña recapitulación sobre cómo se gestan estos acontecimientos, un poco de historia y las similitudes de la violencia sistemática que se ha generado por parte de la policía en el contexto chileno desde el 18 de octubre de 2019. 
    </p>

    <p>
      <strong>
        Nunca más racismo, nunca más discriminación a nuestra gente, todas somos merecedoras de los mismos derechos.
      </strong>
    </p>

    <a 
      href="https://mailchi.mp/f9a83c1f005a/newsletter-sindicato-pi-mayo-4816530" alt="Comunicado completo sobre Black Lives Matter"
    >
      Revisa el comunicado completo
    </a>

  </section>
);

export { Notice };