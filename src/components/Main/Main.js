import { Notice } from 'src/components/Main/Notice/Notice';

import styles from './Main.module.scss';

const Main = () => (
  <main className={styles.main}>
    <p>
      El Sindicato Pi de ThoughtWorks Chile surge por la necesidad de organizarnos como trabajadores de manera colectiva, para representar y defender a los TWers asociados y no asociados, promoviendo la justicia social y económica bajo el marco de nuestra cultura.
    </p>

    <Notice />
  </main>
);

export { Main };