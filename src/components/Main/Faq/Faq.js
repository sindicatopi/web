import { FaqComponent } from "src/components/Faq/FaqComponent";
import { Header } from "src/components/Header/Header";
import { Footer } from "src/components/Footer/Footer";

import styles from "src/Home/Home.module.scss";
import faqStyles from "src/components/Faq/FaqComponent.module.scss";

const Faq = () => (
  <main className={styles.wrapper}>
    <div className={styles.container}>
      <Header />
      <div className={faqStyles.faq}>
        <FaqComponent />
      </div>
    </div>
    <Footer />
  </main>
);

export { Faq };
