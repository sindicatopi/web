//import styles from "./Faq.module.scss";
import Faq from "react-faq-component";

const data = {
  title: "FAQ (¿Lo que no sabías y querías preguntar?)",
  rows: [
    {
      title: "¿Qué es un sidicato de trabajadores?",
      content: `Los sindicatos son organizaciones libremente constituidas por trabajadores del sector privado y/o 
      de las empresas del Estado. Tienen por objetivo, entre otros, asumir la representación y legítima defensa de 
      sus asociados, así como promover los intereses económicos, sociales y culturales de los mismos.
      Como entidad dentro de nuestra oficina, no somos un único canal de comunicación con liderazgo. Si creen que pueden
      resolver sus inquietudes fuera del sindicato, les instamos a que lo hagan.`,
    },
    {
      title: "¿Cómo funciona la organización interna del sindicato? ¿Qué quisieran conseguir?",
      content:
        `Si bien existe una persona directora por temas legales, la organización se lleva de forma colectiva y horizontal
        Personas delegadas, champions o áreas de trabajo aún no están definida pues primero necesitamos definir áreas de foco.
        El sindicato NO reemplazará instancias de feedback con liderazgo u otras TWers, pero puede ayudar a levantar temas 
        comunes que requieran más trabajo o sean más importantes.
        Respecto a negociación colectiva o beneficios específicos, aún se está partiendo pero es perfectamente posible si 
        trabajamos el tema como sindicato e identificamos las cosas que nos gustaría poner sobre la mesa`,
    },
    {
      title: "¿Qué diferencia en la práctica habrá entre las personas que adhieran al sindicato y las que no?",
      content: `Las personas afiliadas podrán recibir beneficios como por ejemplo bonos, otros seguros de salud 
      o regalos. Y serán beneficiadas con el resultado de las negociaciones colectivas. Igualmente contamos con apoyo legal
      por lo que también pueden hacer consultas de ser necesario. `,
    },
    {
      title: "¿Para quién son los beneficios? ¿Habrá segregación?",
      content: `“La extensión de beneficios de una negociación colectiva a trabajadores no sindicalizados requerirá de acuerdo 
      previo entre empleador y sindicatos. Asimismo, el trabajador favorecido con la extensión deberá aceptarlo individualmente 
      y acceder a pagar la cuota sindical, total o parcialmente según se haya acordado. La extensión podrá pactarse dentro de la 
      negociación o a su término.”
      Hay beneficios que no son monetarios y que por su naturaleza son para todos, por ejemplo: Contratación de transporte para 
      equipos que trabajen lejos.
      `,
    },
    {
      title: "¿En qué se diferencia el sindicato del comité paritario?",
      content: `Sindicato de trabajadores: organización que asume la representación y legítima defensa de sus asociados, así como 
      promover los intereses económicos, sociales y culturales de los mismos.

      Comité paritario de higiene y seguridad: organismo técnico de participación entre empresas y trabajadores, para detectar y 
      evaluar los riesgos de accidentes y enfermedades profesionales.
      
      Sin embargo, pueden trabajar en conjunto… “Los sindicatos pueden participar en programas de mejoramiento de los sistemas de 
      prevención de riesgo, tanto de los accidentes de trabajo como de prevención y tratamiento de enfermedades profesionales. Ello, 
      sin perjuicio de la competencia de los Comités Paritarios de Higiene y Seguridad. Además en esta tarea los sindicatos pueden 
      formular planteamiento y peticiones ante éstos Comité y exigir su pronunciamiento.”
      `,
    },
    {
      title: "Soy persona extranjera y tengo contrato con TW Chile ¿Puedo estar sindicalizada?",
      content: `Sí, y es independiente de tu visa`,
    },
  ],
};

const styles = {
  // bgColor: 'white',
  titleTextColor: "black",
  rowTitleColor: "black",
  rowContentColor: "grey",
  // arrowColor: "red",
};

const config = {
  //animate: true,
  arrowIcon: "+",
};

const FaqComponent = () => {

  return (
    <Faq data={data} styles={styles} config={config} />
  );
};

export { FaqComponent };