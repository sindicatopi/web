import Head from 'next/head';

import styles from './Header.module.scss';

const Header = () => (
  <>
    <Head>
      <title>Sindicato PI</title>
    </Head>

    <header className={styles.header}>
      <section className="brand">
        <h1 className={styles.homeLink}> <a href="/" >Hola somos el Sindicato PI </a></h1>
      </section>

      <nav>
        <a
          href="/estatutos/estatutos-sindicato-pi_junio-2020.pdf"
          alt="Descargar estatutos en formato PDF"
        >
          Revisa nuestros estatutos
        </a>
        <a
          href="/faq"
          alt="Preguntas frecuentes"
        >
          Preguntas Frecuentes
        </a>
      </nav>
    </header>
  </>
);

export { Header };