import styles from './Footer.module.scss';

const Footer = () => (
  <footer className={styles.footer}>
    <p>Sindicato PI © 2020 </p>
    <p>Contacto: <a href="mailto:sindicato-pi-tw@thoughtworks.com">sindicato-pi-tw@thoughtworks.com</a></p>
  </footer>
);

export { Footer };