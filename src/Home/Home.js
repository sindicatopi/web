import { Header } from "src/components/Header/Header";
import { Main } from "src/components/Main/Main";
import { Footer } from "src/components/Footer/Footer";

import styles from "./Home.module.scss";

const Home = () => (
  <main className={styles.wrapper}>
    <div className={styles.container}>
      <Header />
      <Main />
    </div>
    <Footer />
  </main>
);

export { Home };
