# Sindicato PI
Este es el repositorio donde se guarda el código fuente del sitio del Sindicato PI de ThoughtWorks Chile. Todo lo que se encuentra aquí es público.

Este sitio está construido con [next.js](https://nextjs.org/) y se aloja gratuitamente en [Vercel](https://vercel.com).

## Desarrollo
Para levantar este proyecto es tan simple como clonar el código y luego instalamos las dependencias con `yarn`.

Para levantar en modo desarrollo ejecutamos `yarn dev` y accedemos a la url `http://localhost:5001`. 

## Producción
La versión que finalmente deployamos es el transpilado para entorno de producción, para generarlo simplemente ejecutamos:

```
yarn build
```
Para probar que se generó de forma correcta podemos ejecutar `yarn start` y se levantará de forma automática en `http://localhost:5001`.